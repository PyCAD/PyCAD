#!/bin/sh

set -e

mkdir -p /etc/letsencrypt/certs

for domain in $RENEWED_DOMAINS; do
        cert_root=/etc/letsencrypt/certs

        rm -f "$daemon_cert_root/$domain.cert"
        rm -f "$daemon_cert_root/$domain.key"

        cp "$RENEWED_LINEAGE/fullchain.pem" "$daemon_cert_root/$domain.cert"
        cp "$RENEWED_LINEAGE/privkey.pem" "$daemon_cert_root/$domain.key"
done

service nginx restart >/dev/null